import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class test5 {
    final static int[] input1 = { 1, 2, 3, 4 };
    final static int[] input2 = { 2, 2, 2, 2, 2, 2 };
    final static int[] input3 = { 0, 4, 3, 4, 5, 8, 6, 8 };
    final static int[] input4 = { 6, 2, 3, 5, 6, 3, 6, 1 };
    final static int[] input5 = { 1, 2, 1, 1, 2, 2, 1, 2 };
    final static int[] input6 = { 1, 2, 3, 4, 5, 6, 7, 8 };
    static boolean hasResult = false;

    public static void solution(int[] input) {
        hasResult = false;
        Arrays.sort(input);
        List<Integer> list = new ArrayList<Integer>();
        int sum = 0;
        for (Integer i : input) {
            sum += i;
        }

        if (sum % 2 != 0 || input.length % 2 != 0) {
            System.out.println("-1");
        } else {
                recursion(0, 0, 0, list, input, sum/2, input.length/2);
        }
        if (!hasResult)
            System.out.println("-1");
    }

    private static void recursion(int sum, int count, int index, List<Integer> list,final int[] input, final int half, final int halfCount) {
        list.add(input[index]);
        sum += input[index];
        count += 1;
        // end recursion
        if (hasResult || index >= input.length || count > halfCount || sum > half) return;
        // end with result
        if (sum == half && count == halfCount) {
            hasResult = true;
            print(list, input);
            return;
        }
        while (index <= input.length - 2 && !hasResult) {
            index++;
            recursion(sum, count, index, list, input, half, halfCount);
            list.remove(list.size() - 1);
        }

    }

    private static void print(List<Integer> list, int[] input) {
        List<Integer> list2 = new ArrayList<Integer>();
        int i = 0, j = 0;
        while (i <= input.length - 1 || j <= list.size() - 1) {
            if (j >= list.size()) {
                list2.add(input[i]);
                i++;
                continue;
            }
            if (list.get(j) > input[i]) {
                list2.add(input[i]);
                i++;
            } else {
                i++;
                j++;
            }
        }
        System.out.println(list.toString());
        System.out.println(list2.toString());
        StringBuilder output = new StringBuilder();
        for (int k = 0; k < list.size(); k++) {
            output.append(list.get(k).toString() + ',');
        }
        for (int k = 0; k < list2.size(); k++) {
            output.append(list2.get(k).toString() + ',');
        }
        output.deleteCharAt(output.length() - 1);
        System.out.println("Output: " + output.toString());
    }

    public static void main(String[] args) {
        solution(input1);
        solution(input2);
        solution(input3);
        solution(input4);
        solution(input5);
        solution(input6);
    }

}
