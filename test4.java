
class test4 {
    final static int[] C1 = { 6, 2, 3, 5 };
    final static int[] C2 = { 1, 2, 1, 2, 1, 2 };
    final static int[] C3 = { 2, 1, 4, 4 };
    final static int[] C4 = { 2, 2, 2, 2, 2, 2 };
    final static String S1 = "aaaa";
    final static String S2 = "aabbcc";
    final static String S3 = "aaaa";
    final static String S4 = "abaccb";

    public static int solution(int[] C, String S) {
        int length = C.length;
        int result = 0;
        StringBuilder result_string = new StringBuilder();
        for (int i = 0; i <= length - 2; i++) {
            if (S.charAt(i) == S.charAt(i + 1)) {
                if (C[i] <= C[i + 1]) {
                    result += C[i];
                } else {
                    result += C[i + 1];
                    C[i + 1] = C[i];
                }
            } else {
                result_string.append(S.charAt(i));
            }
            
        }
        result_string.append(S.charAt(length-1));
        System.out.println("result string is : " + result_string );
        return result;
    }

    public static void main(String[] args) {
        System.out.println("input string is : " + S1);
        System.out.println("min cost is : " + solution(C1, S1));
        System.out.println("input string is : " + S2);
        System.out.println("min cost is : " + solution(C2, S2));
        System.out.println("input string is : " + S3);
        System.out.println("min cost is : " + solution(C3, S3));
        System.out.println("input string is : " + S4);
        System.out.println("min cost is : " + solution(C4, S4));
    }

}
