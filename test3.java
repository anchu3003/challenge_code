import java.util.HashMap;
import java.util.Map;

class test3 {
    final static int[] input = { 51, 71, 17, 42 };
    final static int[] input1 = { 42, 33, 60 };
    final static int[] input2 = { 51, 32, 43 };
    final static int[] input3 = { 2, 2, 2, 2, 2 };

    public static int iterator(int[] input) {
        Map<Integer, int[]> map = new HashMap<Integer, int[]>();
        for (int i : input) {
            if (i < 0 || i > 1000000000 || input.length == 0 || input.length > 200000) {
                System.out.println("Input array out of range");
                return -1;
            }
            char[] charlist = String.valueOf(i).toCharArray();
            int sum = 0;
            for (int j : charlist) {
                sum += Integer.parseInt(Character.toString(j));
            }
            if (map.get(sum) == null) {
                int[] array = { 0, 0 };
                array[1] = i;
                map.put(sum, array);
            } else {
                int[] array = map.get(sum);
                if (i >= array[1]) {
                    array[0] = array[1];
                    array[1] = i;
                } else {
                    if (i >= array[0])
                        array[0] = i;
                }
            }
        }
        final Result max = new Result(0);
        map.forEach((x, y) -> {
            int temp = 0;
            if (y[0] != 0 && y[1] != 0)
                temp = y[0] + y[1];
            if (temp > max.value) {
                max.setValue(temp);
                max.setValue1(y[0]);
                max.setValue2(y[1]);
            }
        });
        if (max.getValue() == 0) {
            System.out.println("No pairs of value satisfy condition");
            return -1;
        } else {
            System.out.println("The pair is " + max.value1 + " " + max.value2);
        }
        return max.getValue();
    }

    public static class Result {
        private int value = 0;
        private int value1 = 0;
        private int value2 = 0;

        Result(int i) {
            this.value = i;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public void setValue1(int value1) {
            this.value1 = value1;
        }

        public void setValue2(int value2) {
            this.value2 = value2;
        }

        public int getValue1() {
            return value1;
        }

        public int getValue2() {
            return value2;
        }

    }

    public static void main(String[] args) {
        System.out.println(iterator(input));
        System.out.println(iterator(input1));
        System.out.println(iterator(input2));
        System.out.println(iterator(input3));
    }

}
